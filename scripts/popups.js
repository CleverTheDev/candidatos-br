var aboutMapBtn = document.querySelector(".about-map")
var aboutMappeaBtn = document.querySelector(".about-mappea")
var closeBtn = document.querySelectorAll(".close-button")

function showModal(kind){
    var modalbg = document.querySelector(".bg-modal");
    modalbg.className = "bg-modal display"
    if (kind === "about-map"){
        var modal = document.querySelector("#about-map-modal")
        modal.className = "modal display"
    }
    else if (kind === "about-mappea"){
        var modal = document.querySelector("#about-mappea-modal")
        modal.className = "modal display"
    }
}
function closeModal(element){
    var modalbg = document.querySelector(".bg-modal");
    modalbg.className = "bg-modal no-display"
    element.className = "modal no-display"
}
aboutMapBtn.addEventListener("click", function(event){
    showModal('about-map');
});

aboutMappeaBtn.addEventListener("click", function(event){
    showModal('about-mappea');
});

closeBtn.forEach((element)=>{
    element.addEventListener("click", function(event){
        closeModal(event.target.parentNode);   
    })
})