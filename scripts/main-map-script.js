var highlightLayer;
function highlightFeature(e) {
    highlightLayer = e.target;
    highlightLayer.openPopup();
}
var map = L.map('map', {
    zoomControl:true, 
    maxZoom:8, 
    minZoom:4,
    maxBounds:[[-34.726753471025006,-88.37393353679808],[6.247416553958239,-14.452424061201908]]
}).fitBounds([[-34.726753471025006,-88.37393353679808],[6.247416553958239,-14.452424061201908]]);
var hash = new L.Hash(map);
map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a>');
var bounds_group = new L.featureGroup([]);
function setBounds() {
}
function pop_DeputadoEstadual_0(feature, layer) {
    layer.on({
        mouseout: function(e) {
            if (typeof layer.closePopup == 'function') {
                layer.closePopup();
            } else {
                layer.eachLayer(function(feature){
                    feature.closePopup()
                });
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <td colspan="2"><strong>NM_ESTADO</strong><br />' + (feature.properties['NM_ESTADO'] !== null ? Autolinker.link(String(feature.properties['NM_ESTADO'])) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2"><strong>Cand_DepEs</strong><br />' + (feature.properties['Cand_DepEs'] !== null ? Autolinker.link(String(feature.properties['Cand_DepEs'])) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_DeputadoEstadual_0_0(feature) {
    if (feature.properties['Cand_DepEs'] >= 219.000000 && feature.properties['Cand_DepEs'] <= 234.000000 ) {
        return {
        pane: 'pane_DeputadoEstadual_0',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(239,236,255,1.0)',
    }
    }
    if (feature.properties['Cand_DepEs'] >= 234.000000 && feature.properties['Cand_DepEs'] <= 355.000000 ) {
        return {
        pane: 'pane_DeputadoEstadual_0',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(202,226,237,1.0)',
    }
    }
    if (feature.properties['Cand_DepEs'] >= 355.000000 && feature.properties['Cand_DepEs'] <= 535.000000 ) {
        return {
        pane: 'pane_DeputadoEstadual_0',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(166,216,219,1.0)',
    }
    }
    if (feature.properties['Cand_DepEs'] >= 535.000000 && feature.properties['Cand_DepEs'] <= 767.000000 ) {
        return {
        pane: 'pane_DeputadoEstadual_0',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(129,206,200,1.0)',
    }
    }
    if (feature.properties['Cand_DepEs'] >= 767.000000 && feature.properties['Cand_DepEs'] <= 1392.000000 ) {
        return {
        pane: 'pane_DeputadoEstadual_0',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(93,195,182,1.0)',
    }
    }
    if (feature.properties['Cand_DepEs'] >= 1392.000000 && feature.properties['Cand_DepEs'] <= 2465.000000 ) {
        return {
        pane: 'pane_DeputadoEstadual_0',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(56,185,164,1.0)',
    }
    }
}
map.createPane('pane_DeputadoEstadual_0');
map.getPane('pane_DeputadoEstadual_0').style.zIndex = 400;
map.getPane('pane_DeputadoEstadual_0').style['mix-blend-mode'] = 'normal';
var layer_DeputadoEstadual_0 = new L.geoJson(json_DeputadoEstadual_0, {
    attribution: '<a href=""></a>',
    pane: 'pane_DeputadoEstadual_0',
    onEachFeature: pop_DeputadoEstadual_0,
    style: style_DeputadoEstadual_0_0,
});
bounds_group.addLayer(layer_DeputadoEstadual_0);
map.addLayer(layer_DeputadoEstadual_0);
function pop_DeputadoFederal_1(feature, layer) {
    layer.on({
        mouseout: function(e) {
            if (typeof layer.closePopup == 'function') {
                layer.closePopup();
            } else {
                layer.eachLayer(function(feature){
                    feature.closePopup()
                });
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <td colspan="2"><strong>NM_ESTADO</strong><br />' + (feature.properties['NM_ESTADO'] !== null ? Autolinker.link(String(feature.properties['NM_ESTADO'])) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2"><strong>Cand_DepFe</strong><br />' + (feature.properties['Cand_DepFe'] !== null ? Autolinker.link(String(feature.properties['Cand_DepFe'])) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_DeputadoFederal_1_0(feature) {
    if (feature.properties['Cand_DepFe'] >= 83.000000 && feature.properties['Cand_DepFe'] <= 88.000000 ) {
        return {
        pane: 'pane_DeputadoFederal_1',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(239,236,255,1.0)',
    }
    }
    if (feature.properties['Cand_DepFe'] >= 88.000000 && feature.properties['Cand_DepFe'] <= 170.000000 ) {
        return {
        pane: 'pane_DeputadoFederal_1',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(203,226,237,1.0)',
    }
    }
    if (feature.properties['Cand_DepFe'] >= 170.000000 && feature.properties['Cand_DepFe'] <= 267.000000 ) {
        return {
        pane: 'pane_DeputadoFederal_1',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(166,216,219,1.0)',
    }
    }
    if (feature.properties['Cand_DepFe'] >= 267.000000 && feature.properties['Cand_DepFe'] <= 503.000000 ) {
        return {
        pane: 'pane_DeputadoFederal_1',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(129,206,201,1.0)',
    }
    }
    if (feature.properties['Cand_DepFe'] >= 503.000000 && feature.properties['Cand_DepFe'] <= 1154.000000 ) {
        return {
        pane: 'pane_DeputadoFederal_1',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(92,195,182,1.0)',
    }
    }
    if (feature.properties['Cand_DepFe'] >= 1154.000000 && feature.properties['Cand_DepFe'] <= 1686.000000 ) {
        return {
        pane: 'pane_DeputadoFederal_1',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(56,185,164,1.0)',
    }
    }
}
map.createPane('pane_DeputadoFederal_1');
map.getPane('pane_DeputadoFederal_1').style.zIndex = 401;
map.getPane('pane_DeputadoFederal_1').style['mix-blend-mode'] = 'normal';
var layer_DeputadoFederal_1 = new L.geoJson(json_DeputadoFederal_1, {
    attribution: '<a href=""></a>',
    pane: 'pane_DeputadoFederal_1',
    onEachFeature: pop_DeputadoFederal_1,
    style: style_DeputadoFederal_1_0,
});
bounds_group.addLayer(layer_DeputadoFederal_1);
function pop_Senador_2(feature, layer) {
    layer.on({
        mouseout: function(e) {
            if (typeof layer.closePopup == 'function') {
                layer.closePopup();
            } else {
                layer.eachLayer(function(feature){
                    feature.closePopup()
                });
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <td colspan="2"><strong>NM_ESTADO</strong><br />' + (feature.properties['NM_ESTADO'] !== null ? Autolinker.link(String(feature.properties['NM_ESTADO'])) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2"><strong>Cand_Sen</strong><br />' + (feature.properties['Cand_Sen'] !== null ? Autolinker.link(String(feature.properties['Cand_Sen'])) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_Senador_2_0(feature) {
    if (feature.properties['Cand_Sen'] >= 6.000000 && feature.properties['Cand_Sen'] <= 7.000000 ) {
        return {
        pane: 'pane_Senador_2',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(239,236,255,1.0)',
    }
    }
    if (feature.properties['Cand_Sen'] >= 7.000000 && feature.properties['Cand_Sen'] <= 10.000000 ) {
        return {
        pane: 'pane_Senador_2',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(203,226,237,1.0)',
    }
    }
    if (feature.properties['Cand_Sen'] >= 10.000000 && feature.properties['Cand_Sen'] <= 12.000000 ) {
        return {
        pane: 'pane_Senador_2',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(166,216,219,1.0)',
    }
    }
    if (feature.properties['Cand_Sen'] >= 12.000000 && feature.properties['Cand_Sen'] <= 15.000000 ) {
        return {
        pane: 'pane_Senador_2',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(129,206,201,1.0)',
    }
    }
    if (feature.properties['Cand_Sen'] >= 15.000000 && feature.properties['Cand_Sen'] <= 18.000000 ) {
        return {
        pane: 'pane_Senador_2',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(92,195,182,1.0)',
    }
    }
    if (feature.properties['Cand_Sen'] >= 18.000000 && feature.properties['Cand_Sen'] <= 20.000000 ) {
        return {
        pane: 'pane_Senador_2',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(56,185,164,1.0)',
    }
    }
}
map.createPane('pane_Senador_2');
map.getPane('pane_Senador_2').style.zIndex = 402;
map.getPane('pane_Senador_2').style['mix-blend-mode'] = 'normal';
var layer_Senador_2 = new L.geoJson(json_Senador_2, {
    attribution: '<a href=""></a>',
    pane: 'pane_Senador_2',
    onEachFeature: pop_Senador_2,
    style: style_Senador_2_0,
});
bounds_group.addLayer(layer_Senador_2);
function pop_Governador_3(feature, layer) {
    layer.on({
        mouseout: function(e) {
            if (typeof layer.closePopup == 'function') {
                layer.closePopup();
            } else {
                layer.eachLayer(function(feature){
                    feature.closePopup()
                });
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <td colspan="2"><strong>NM_ESTADO</strong><br />' + (feature.properties['NM_ESTADO'] !== null ? Autolinker.link(String(feature.properties['NM_ESTADO'])) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2"><strong>Cand_Gov</strong><br />' + (feature.properties['Cand_Gov'] !== null ? Autolinker.link(String(feature.properties['Cand_Gov'])) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_Governador_3_0(feature) {
    if (feature.properties['Cand_Gov'] >= 5.000000 && feature.properties['Cand_Gov'] <= 6.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(239,236,255,1.0)',
    }
    }
    if (feature.properties['Cand_Gov'] >= 6.000000 && feature.properties['Cand_Gov'] <= 7.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(213,229,242,1.0)',
    }
    }
    if (feature.properties['Cand_Gov'] >= 7.000000 && feature.properties['Cand_Gov'] <= 8.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(187,222,229,1.0)',
    }
    }
    if (feature.properties['Cand_Gov'] >= 8.000000 && feature.properties['Cand_Gov'] <= 9.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(161,214,216,1.0)',
    }
    }
    if (feature.properties['Cand_Gov'] >= 9.000000 && feature.properties['Cand_Gov'] <= 10.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(134,207,203,1.0)',
    }
    }
    if (feature.properties['Cand_Gov'] >= 10.000000 && feature.properties['Cand_Gov'] <= 11.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(108,200,190,1.0)',
    }
    }
    if (feature.properties['Cand_Gov'] >= 11.000000 && feature.properties['Cand_Gov'] <= 12.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(82,193,177,1.0)',
    }
    }
    if (feature.properties['Cand_Gov'] >= 12.000000 && feature.properties['Cand_Gov'] <= 13.000000 ) {
        return {
        pane: 'pane_Governador_3',
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(56,185,164,1.0)',
    }
    }
}
map.createPane('pane_Governador_3');
map.getPane('pane_Governador_3').style.zIndex = 403;
map.getPane('pane_Governador_3').style['mix-blend-mode'] = 'normal';
var layer_Governador_3 = new L.geoJson(json_Governador_3, {
    attribution: '<a href=""></a>',
    pane: 'pane_Governador_3',
    onEachFeature: pop_Governador_3,
    style: style_Governador_3_0,
});
bounds_group.addLayer(layer_Governador_3);
var baseMaps = {};
L.control.layers(baseMaps,{'Governador<br /><table><tr><td style="text-align: center;"><img src="legend/Governador_3_5006000.png" /></td><td> 5.00 - 6.00 </td></tr><tr><td style="text-align: center;"><img src="legend/Governador_3_6007001.png" /></td><td> 6.00 - 7.00 </td></tr><tr><td style="text-align: center;"><img src="legend/Governador_3_7008002.png" /></td><td> 7.00 - 8.00 </td></tr><tr><td style="text-align: center;"><img src="legend/Governador_3_8009003.png" /></td><td> 8.00 - 9.00 </td></tr><tr><td style="text-align: center;"><img src="legend/Governador_3_90010004.png" /></td><td> 9.00 - 10.00 </td></tr><tr><td style="text-align: center;"><img src="legend/Governador_3_100011005.png" /></td><td> 10.00 - 11.00 </td></tr><tr><td style="text-align: center;"><img src="legend/Governador_3_110012006.png" /></td><td> 11.00 - 12.00 </td></tr><tr><td style="text-align: center;"><img src="legend/Governador_3_120013007.png" /></td><td> 12.00 - 13.00 </td></tr></table>': layer_Governador_3,'Senador<br /><table><tr><td style="text-align: center;"><img src="legend/Senador_2_670.png" /></td><td> 6 - 7 </td></tr><tr><td style="text-align: center;"><img src="legend/Senador_2_7101.png" /></td><td> 7 - 10 </td></tr><tr><td style="text-align: center;"><img src="legend/Senador_2_10122.png" /></td><td> 10 - 12 </td></tr><tr><td style="text-align: center;"><img src="legend/Senador_2_12153.png" /></td><td> 12 - 15 </td></tr><tr><td style="text-align: center;"><img src="legend/Senador_2_15184.png" /></td><td> 15 - 18 </td></tr><tr><td style="text-align: center;"><img src="legend/Senador_2_18205.png" /></td><td> 18 - 20 </td></tr></table>': layer_Senador_2,'Deputado Federal<br /><table><tr><td style="text-align: center;"><img src="legend/DeputadoFederal_1_83880.png" /></td><td> 83 - 88 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoFederal_1_881701.png" /></td><td> 88 - 170 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoFederal_1_1702672.png" /></td><td> 170 - 267 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoFederal_1_2675033.png" /></td><td> 267 - 503 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoFederal_1_50311544.png" /></td><td> 503 - 1154 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoFederal_1_115416865.png" /></td><td> 1154 - 1686 </td></tr></table>': layer_DeputadoFederal_1,'Deputado Estadual<br /><table><tr><td style="text-align: center;"><img src="legend/DeputadoEstadual_0_2192340.png" /></td><td> 219 - 234 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoEstadual_0_2343551.png" /></td><td> 234 - 355 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoEstadual_0_3555352.png" /></td><td> 355 - 535 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoEstadual_0_5357673.png" /></td><td> 535 - 767 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoEstadual_0_76713924.png" /></td><td> 767 - 1392 </td></tr><tr><td style="text-align: center;"><img src="legend/DeputadoEstadual_0_139224655.png" /></td><td> 1392 - 2465 </td></tr></table>': layer_DeputadoEstadual_0,},{collapsed:false}).addTo(map);
setBounds();
map.addControl(new L.Control.Search({
    layer: layer_DeputadoEstadual_0,
    initial: false,
    hideMarkerOnCollapse: false,
    propertyName: 'NM_ESTADO'}));
document.getElementsByClassName('search-button')[0].className +=
    ' fa fa-binoculars';

