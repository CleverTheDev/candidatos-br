/*<a class="leaflet-control-layers-toggle" href="#" title="Layers"></a>*/

var legendArea = document.querySelector(".leaflet-top.leaflet-right");
var legend = legendArea.firstChild;
legend.removeChild(legend.firstChild);
legend.className = "leaflet-control-layers leaflet-control"
var toggleButton = document.createElement("a");
toggleButton.className = "leaflet-control-layers-toggle"
toggleButton.href = "#"
toggleButton.title = "Layers"
toggleButton.addEventListener("click", ()=>{
    legend.className =  legend.className + " leaflet-control-layers-expanded";
})
legend.appendChild(toggleButton);


var find = function(element){
    if(element){
        return(element === legendArea?
        true:
        find(element.parentElement))
    }
    return false
}
document.addEventListener("click", function(event){
    if(event.target === toggleButton){
        return false;
    }
    var target = find(event.target);
    if(target){
        return false;
    }
    legend.className = "leaflet-control-layers leaflet-control";
    return true;
})
